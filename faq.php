<?php
/**
 * Plugin Name: FAQ
 * Description: Plugin of FAQ
 * Author: Author
 * Version: 1.0.1
 * Directory: https://camaleaun.bitbucket.io/faq
 */

function faq_register_cpt() {
	$args = array(
		'public'       => false,
		'show_ui'      => true,
		'label'        => __( 'FAQs', 'faq' ),
		'hierarchical' => true,
		'supports'     => array(
			'title',
			'editor',
			'author',
			'page-attributes',
		),
		'menu_icon'    => 'dashicons-editor-help',
	);
	register_post_type( 'faq', $args );
}
add_action( 'init', 'faq_register_cpt' );

require_once dirname( __FILE__ ) . '/seld/class-selfdirectory.php';
add_action( 'selfd_register', 'faq_register_selfdirectory' );
function faq_register_selfdirectory() {
	selfd( __FILE__ );
}
